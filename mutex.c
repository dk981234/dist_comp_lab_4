#include <stdlib.h>
#include "pa2345.h"
#include "mutex.h"
#include "ipc.h"
#include "extra_ipc.h"
#include "lamport_time.h"
#include "queue.h"
#include "request_table.h"

int MUTEX_ALLOWED = 0;

void mutex_allowed() { MUTEX_ALLOWED = 1; }

int is_mutex_allowed() { return MUTEX_ALLOWED; }

int request_cs(const void * self){
    if(!is_mutex_allowed())
        return -1;
    struct mutex* mutex = (struct mutex*)self;

    timestamp_t request_time = get_lamport_time();  
    Message *msg = malloc(sizeof(Message));
    init_message_header(msg, 0, CS_REQUEST, request_time);
    send_multicast(get_pipes(mutex)[1], msg);
   //Занести себя
    add(init_request_value(mutex->id, request_time), get_table(mutex));
    int reply_counter = 0;
    while(reply_counter < get_process_quantity(mutex) - 1 || min_value(get_table(mutex), get_process_quantity(mutex)).id != mutex->id) {
        local_id src_id = (local_id) receive_any(get_pipes(mutex)[0], msg);
        compare_lamport_time(msg->s_header.s_local_time);
        get_lamport_time();
        switch (msg->s_header.s_type) {
            case CS_REQUEST:
                add(init_request_value(src_id, msg->s_header.s_local_time), get_table(mutex));
                init_message_header(msg, 0, CS_REPLY, get_lamport_time());
                send(get_pipes(mutex)[1], src_id, msg);
                break;
            case CS_REPLY:
                reply_counter++;
                break;
            case CS_RELEASE:
                table_remove(src_id, get_table(mutex));
                break;
            case DONE:
                mutex->done_counter++;
                break;
            default:
                break;
        }
    }
    free(msg);
    return 0;
}

int release_cs(const void * self){
    if(!is_mutex_allowed())
        return -1;
    struct mutex* mutex = (struct mutex*)self;
    table_remove(mutex->id, get_table(mutex));
    Message * msg = malloc(sizeof(Message));
    init_message_header(msg, 0, CS_RELEASE, get_lamport_time());
    send_multicast(get_pipes(mutex)[1], msg);
    return 0;
}

int * get_table(struct mutex *this ){
    return (this->table);
}
local_id get_id(struct mutex *this) { return this->id; }
int** get_pipes(struct mutex *this) { return this->pipes; }
int get_process_quantity(struct mutex *this) { return this->process_quantity; }

void init_mutex(struct mutex *this, local_id id, int **pipes, int process_quantity){
    this->done_counter = 0;
    this->id = id;
    this->pipes = pipes;
    this->process_quantity = process_quantity;
}
