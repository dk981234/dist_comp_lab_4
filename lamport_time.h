#ifndef DIST_COMP_LAB_3_LAMPORT_TIME_H
#define DIST_COMP_LAB_3_LAMPORT_TIME_H

#include "ipc.h"

enum {
    MAX_T = 255 ///< max possible value of timestamp returned by get_lamport_time()
    ///< or get_physical_time()
};

/**
 * Returs the value of Lamport's clock.
 */
timestamp_t get_lamport_time();


void compare_lamport_time(timestamp_t s_time);

#endif
