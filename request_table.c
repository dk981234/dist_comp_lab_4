#include "request_table.h"
#include <stdlib.h>
#include "ipc.h"
int isChange = 1;
request_value prev_value;
void add(request_value value, int * time_table){
    time_table[value.id] = value.time;
    isChange = 1;
}
void table_remove(local_id id, int * table){
    table [id] = 256;
    isChange = 1;
}
request_value min_value(int * table, int max){
    if(!isChange){
        return prev_value;
    }
    timestamp_t min_time = 256;
    local_id min_id = max + 1;
    for(int i = 1; i <= max; i++){
        if(min_time > table[i]){
            min_time = table[i];
            min_id = i;
        }
        else if(min_time == table[i]){
            if(min_id > i) min_id = i; 
        }
        }
    prev_value = init_request_value(min_id, min_time);
    isChange = 0;
    return prev_value;
}
request_value init_request_value(local_id id,timestamp_t time){
    request_value value;
    value.id = id;
    value.time = time;
    return  value;
}
