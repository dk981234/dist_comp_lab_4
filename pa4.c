#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/wait.h>

#include "getopt.h"
#include "ipc.h"
#include "pipes.h"
#include "extra_ipc.h"
#include "pa2345.h"
#include "logs.h"
#include "lamport_time.h"
#include "mutex.h"

local_id PROC_ID = PARENT_ID;

void receive_all_starts(int *read_pipes, local_id max_id){
    Message *msg = malloc(sizeof(Message));
  
    timestamp_t time;
    
    for(int i = 0; i <= max_id; i++){
        if(i == PARENT_ID || i == PROC_ID ) continue;
        receive(read_pipes, i, msg);
        compare_lamport_time(msg->s_header.s_local_time);
        if (msg->s_header.s_type == STARTED)
        {
            time = get_lamport_time();
        }
        else {
            i--;
        }
    }
    logging(log_received_all_started_fmt, time, PROC_ID); 
}



void start(int *read_pipes, int *write_pipes, local_id max_id) {

    logging(log_started_fmt, 0, PROC_ID, getpid(), getppid(), 0);

    timestamp_t time = get_lamport_time();

    Message * msg = malloc(sizeof(Message));
    uint16_t size_of_paylaod = sprintf(msg->s_payload, log_started_fmt, time, PROC_ID, getpid(), getppid(), 0);
    init_message_header(msg, size_of_paylaod, STARTED, time);

    send_multicast(write_pipes, msg);

    receive_all_starts(read_pipes, max_id);

    free(msg);
}



void done(int *write_pipes) {

    timestamp_t time = get_lamport_time();

    Message * msg = malloc(sizeof(Message));
    uint16_t size_of_paylaod = sprintf(msg->s_payload, log_done_fmt, time, PROC_ID, 0);
    init_message_header(msg, size_of_paylaod, DONE, time);

    send_multicast(write_pipes, msg);

    free(msg);
}

void K(local_id max_id, int **pipes) {

    receive_all_starts(pipes[0], max_id);

    Message * msg = malloc(sizeof(Message));

    int done_counter = 0;

    while (done_counter < max_id) {
        receive_any(pipes[0], msg);
        compare_lamport_time(msg->s_header.s_local_time);
        timestamp_t time = get_lamport_time();
        switch (msg->s_header.s_type) {
            case DONE:
                done_counter++;
                if(done_counter == max_id)
                    logging(log_received_all_done_fmt, time, PROC_ID);
                break;
            default:
                break;
        }
    }

    free(msg);
}

void paylaod(local_id id, int iteration, int out_of){
    char str[128];
    sprintf(str, log_loop_operation_fmt, id, iteration, out_of);
    print(str);

    logging(log_loop_operation_fmt, id, iteration, out_of);
}

void C(local_id max_id, int **pipes) {
    start(pipes[0], pipes[1], max_id);
    int done_counter = 0;
    Message *msg = malloc(sizeof(Message));
    const int print_payload_times = PROC_ID * 5;
    struct mutex* mutex = malloc(sizeof(struct mutex));
    init_mutex(mutex, PROC_ID, pipes, max_id);
    for(int print_payload_counter = 1; print_payload_counter <= print_payload_times; print_payload_counter++){
        request_cs(mutex);
        paylaod(PROC_ID, print_payload_counter, print_payload_times);
        release_cs(mutex);
    }
    done_counter+=mutex->done_counter;
    done(pipes[1]);
    while (done_counter != max_id - 1) {
        local_id src_id = (local_id) receive_any(pipes[0], msg);
        compare_lamport_time(msg->s_header.s_local_time);
        get_lamport_time();
        switch (msg->s_header.s_type) {
            case CS_REQUEST:
                init_message_header(msg, 0, CS_REPLY, get_lamport_time());
                send(pipes[1], src_id, msg);
                break;
            case DONE:
                ++done_counter;
                break;
            case CS_RELEASE:
            default:
                break;
        }
    }

    logging(log_received_all_done_fmt, get_lamport_time(), PROC_ID);

    free(msg);
}


void parse_args(int argc, char *argv[], int  *use_mutex, int *children_count) {
    struct option long_options[] = {{"mutexl", no_argument, 0, 0}};
    int option_index = 0;
    int opt;
    while ((opt = getopt_long(argc, argv, "p:", long_options, &option_index)) != -1) {
    switch (opt) {
        case 0:
            *use_mutex = 1;
        break;
        case 'p':
    *children_count = atoi(optarg);
    break;
    default:
        exit(EXIT_FAILURE);
}
}
}
int main(int argc, char *argv[]) {
    int n;
    int use_mutex;
    parse_args(argc, argv, &use_mutex, &n);
	if(use_mutex) mutex_allowed();
    if (open_logfiles())
        exit(-1);
    init_pipes_buffer(n);
    for (local_id i = 1; i <= n; i++) {
        if (!fork()) {
            PROC_ID = i;
            break;
            }
    }
    int **pipes = select_pipes(PROC_ID);
    close_pipes(n, PROC_ID);
     if (PROC_ID != PARENT_ID) {
                     C((local_id) n, pipes);
            } 
    else {
        K((local_id) n, pipes);
        while (wait(NULL) > 0);
    }
    return 0;
}
