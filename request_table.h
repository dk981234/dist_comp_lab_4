#ifndef DIST_COMP_LAB_4_REQUEST_TABLE_H
#define DIST_COMP_LAB_4_REQUEST_TABLE_H
#include "ipc.h"
typedef struct request_value{
    timestamp_t time;
    local_id id;
} request_value;
void add(request_value value, int *table);
void table_remove(local_id id, int * table);
request_value min_value(int * table, int max);
request_value init_request_value(local_id id,timestamp_t time);
#endif
