#include <stdio.h>
#include <stdarg.h>
#include "common.h"
FILE * events;
FILE * pipes;

int open_logfiles()
{
    pipes = fopen(pipes_log, "a");
    events = fopen(events_log, "a");
    setvbuf(pipes, NULL, _IOLBF, 0);
    setvbuf(events, NULL, _IOLBF, 0);
    
    return (events == NULL || pipes == NULL) ? -1 : 0;
}

void logging(const char *format, ...)
{
    va_list args_file;
    va_start (args_file, format);
    vfprintf(events, format, args_file);
    va_end (args_file);
}

void log_pipes(const char *format, ...)
{    
    va_list args_file;
    va_start (args_file, format);
    vfprintf(pipes, format, args_file);
    va_end (args_file);

}

