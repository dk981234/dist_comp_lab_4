#ifndef DIST_COMP_LAB_4_MUTEX_H
#define DIST_COMP_LAB_4_MUTEX_H

#include "queue.h"
#include "request_table.h"
struct mutex {
    int done_counter;
    local_id id;
    int **pipes;
    int process_quantity;
    int  table [11];
};
int * get_table(struct mutex *this);
local_id get_id(struct mutex *this);
int** get_pipes(struct mutex *this);
int get_process_quantity(struct mutex *this);
struct queue* get_message_queue(struct mutex *this);
struct request_queue* get_waiting_queue(struct mutex *this);

void init_mutex(struct mutex *this, local_id id, int **pipes, int process_quantity);

void mutex_allowed();

int is_mutex_allowed();

#endif
